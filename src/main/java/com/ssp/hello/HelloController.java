package com.ssp.hello;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;

@RestController
public class HelloController {

    @GetMapping("/hello")
    public String sayHello(@RequestParam(defaultValue = "World", required = false) String name){
        return String.format("Hello %s from Spring", name);
    }

    @GetMapping("/compute")
    public String compute(@RequestParam(defaultValue = "", required = false) Map<String,String> params){
        String num1 = params.get("num1");
        String num2 = params.get("num2");
        String operator = params.get("operator");
        BigDecimal result = new BigDecimal(num1).add(new BigDecimal(num2));

        return result.toString();

    }

    @GetMapping("/countVowels")
    public String countVowels(@RequestParam(defaultValue = "", required = false) String word){
        int vowels = 0;
        for (int i = 0; i < word.length(); i++)
        {
            if (word.charAt(i) == 'a' || word.charAt(i) == 'e' || word.charAt(i) == 'i'
                    || word.charAt(i) == 'o' || word.charAt(i) == 'u')
            {
                vowels++;
            }
        }
        return Integer.toString(vowels);
    }

    @PostMapping("/searchAndReplace")
    public String searchAndReplace(@RequestParam(defaultValue = "", required = false) Map<String,String> params){

        String phrase = params.get("string");
        String search = params.get("search");
        String replace = params.get("replace");

        phrase = phrase.replaceAll(search, replace);

        return phrase;
    }

}
