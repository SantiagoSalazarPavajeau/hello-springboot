package com.ssp.hello;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(HelloController.class)
public class HelloControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void sayHello_noArgs_returnsHelloWorld() throws Exception {
        mockMvc.perform(get("/hello"))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello World from Spring"));
    }

    @Test
    void sayHello_withArgs_returnsHelloNameFromSpring() throws Exception {
        mockMvc.perform(get("/hello?name=Santi"))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello Santi from Spring"));
    }

    @Test
    void sayHello_withArgs_returnsComputedValueFromNumbersAndOperator() throws Exception{
        mockMvc.perform(get("/compute?num1=1&num2=2&operator=+"))
                .andExpect(status().isOk())
                .andExpect(content().string("3"));
    }


    @Test
    void sayHello_withArgs_returnsCountOfVowelsInString() throws Exception{
        mockMvc.perform(get("/countVowels?word=springboot"))
                .andExpect(status().isOk())
                .andExpect(content().string("3"));
    }

    @Test
    void sayHello_postRequest_acceptsStringAndTwoQueriesForSearchAndReplace() throws Exception{

        mockMvc.perform(post("/searchAndReplace?string=Ilikepotatoes&search=potatoes&replace=tomatoes"))
                .andExpect(status().isOk())
                .andExpect(content().string("Iliketomatoes"));
    }


}
